/**
 *
Simple websocket for iot protocal
Pratin Sonnekorn

Message format
  login :  "key:{--channel-key--}"
  sendTo:  "sendto:{----sendtokey----}---endkey--- {"water":"on"}"
  isonline: "isonline:{----sendtokey----}---endkey---"


 */




var WebSocketServer = require('ws').Server
var wss = new WebSocketServer({ port: 7777 });

var clients = [];

wss.on('connection', function connection(ws) {

  ws.on('close',function(){

    for (var i = 0; i < clients.length; i++) {
        if (clients[i].ws == ws) {
          console.log("disconnect from : " + clients[i].key);
          clients.splice(i, 1);
        }
      }

  });

  ws.on('message', function incoming(message) {

      var keyIndex = message.indexOf('key:');
      if(keyIndex == 0){
        var key = message.substring(4,message.length);
        for (var i = 0; i < clients.length; i++) {
          if (clients[i].key == key) {
            if (i > -1) {
                  clients.splice(i, 1);
              }
          }
        }
        console.log("client added ->" + key);
        clients.push({
          key : key,
          ws : ws ,
          ip : ws.upgradeReq.url
        });
      }


      var messageKeyIndex = message.indexOf('sendto:');
      if(messageKeyIndex == 0){
        var eok = message.indexOf('---endkey---');
        var sendto_key = message.substring(7,eok);
        var send_message = message.substring(eok+13,message.length);
        for (var i = 0; i < clients.length; i++) {
          if (clients[i].key == sendto_key) {
            console.log("send data to " + sendto_key);
            clients[i].ws.send(send_message);
          }
        }
      }

      var getIpKeyIndex = message.indexOf('getip:');
      if(getIpKeyIndex == 0){
        var eok = message.indexOf('---endkey---');
        var sendto_key = message.substring('getip:'.length,eok);
        for (var i = 0; i < clients.length; i++) {
          if (clients[i].key == sendto_key) {
            ws.send(clients[i].ip);
            //console.log("get ip" + clients[i].ip);
          }
        }
      }

      var statusKeyIndex = message.indexOf('isonline:');
      if(statusKeyIndex == 0){
        var eok = message.indexOf('---endkey---');
        var clientKey = message.substring(9,eok);
        var isOnline = false ;
        for (var i = 0; i < clients.length; i++) {
          if (clients[i].key == clientKey) {
            isOnline = true ;
            break;
          }
        }
        if(isOnline){
          ws.send("device_status:online");
        }else{
          ws.send("device_status:offline");
        }

      }


  });

});
