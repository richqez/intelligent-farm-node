var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductvitySchema = new Schema({
  value: { type: Number },
  date: { type: Date },
  channel : {
    type: Schema.Types.ObjectId ,
    ref : 'Channel'
  }
},{
  timestamps:true
});

ProductvitySchema.methods.toJSON = function() {
  var obj = this.toObject()
  delete obj.updatedAt
  return obj
}


module.exports = mongoose.model('Productvity', ProductvitySchema);
