var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FeedSchema = new Schema({
  humidity: {
        type: String,
    },
  temperature: {
        type: String,
        required: false,
    },
  channel : { type: Schema.Types.ObjectId , ref : 'Channel'}

},{
  timestamps:true
});

FeedSchema.index({ timestamps: 1 });

FeedSchema.methods.toJSON = function() {
  var obj = this.toObject()
  delete obj.updatedAt
  // delete obj.channel
  return obj
}


module.exports = mongoose.model('Feed', FeedSchema);
