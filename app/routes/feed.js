var Feed = require('../models/feed');
var express = require('express');
var userAuth = require('../lib/userAuth');
var jwt = require('jwt-simple');
var passport = require('passport');
var dateUtils = require('../lib/dateUtils');
var async = require("async");


module.exports = (function(){

	'use strict';

	var feed = express.Router();

  /**
   * insert data to feed
   */
  feed.post('/feed/channel/:_id',passport.authenticate('jwt', { session: false}),function(req,res){

    var newFeed = new Feed({
      humidity : req.body.humidity ,
      temperature : req.body.temperature,
      channel : req.body.channel
    });

    newFeed.save(function(err,_newFeed){
      if (err) {
        throw err
      }else{
        res.json(_newFeed);
      }
    })
  });

  /**
   * find by channel id
   */
  feed.get('/feed/channel/:_id',passport.authenticate('jwt', { session: false}),function(req,res){
    Feed.find({ channel : req.params._id } , function(err,feeds){
      res.json(feeds);
    });
  })




	// dd mm yyy
  feed.get('/feed/channel/:channel/date/:start/:end',passport.authenticate('jwt', { session: false}),function(req,res){

    var token = userAuth.verify(req.headers);

    var  start = new Date(req.params.start).toISOString();
    var  end = new Date(req.params.end).toISOString();

    Feed.find({channel:req.params.channel,"createdAt":{'$gte':start,'$lt':end}}, function(err, feed){

        res.json(feed);
      });
    });



	/**
	 * getforchart
	 */
	 feed.get('/feed/channel/:_id/forchart/',function(req,res){
		 Feed.find({ channel : req.params._id })
		 	.sort({ createdAt :  -1 })
			.limit(5)
			.exec(function(err,feeds){

				var humidity = [];
				var temperature = [];
				var labels = [];


				feeds.sort(function(o1,o2){
					return new Date(o1.createdAt) - new Date(o2.createdAt);
				});

				feeds.forEach(function(feed){
					humidity.push(feed.humidity);
					temperature.push(feed.temperature);
					labels.push(dateUtils.toShow(feed.createdAt));
				});

				res.json({ humidity:humidity, temperature:temperature , labels : labels});
			});
	 });



	 feed.get('/feed/channel/:_id/forchart/v2',function(req,res){
		Feed.find({ channel : req.params._id })
		 .sort({ createdAt :  -1 })
		 .limit(40)
		 .exec(function(err,feeds){


				var series = [] ;
				var categories = [];
				var humidity = [];
				var temperature = [];


			 feeds.sort(function(o1,o2){
				 return new Date(o1.createdAt) - new Date(o2.createdAt);
			 });

			 feeds.forEach(function(feed){
				 humidity.push(parseFloat(feed.humidity));
				 temperature.push(parseFloat(feed.temperature));
				 categories.push(dateUtils.toShow(feed.createdAt));

			 });

			 res.json({
				 categories : categories ,
				 series:[
					 {
						 name : "humidity" ,
						 data : humidity ,
					 },
					 {
						 name : "temperature" ,
						 data : temperature ,
					 }

				 ]
			 });
		 });
	});

  /**
   *  find by date range
   */







  return feed;



})();
