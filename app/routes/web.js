
var passport = require('passport');
var express = require('express');
var User = require('../models/users');
var Channel = require('../models/channel');
var Feed = require('../models/feed');
var Productvity = require('../models/productvity');
var dateUtils = require('../lib/dateUtils');
var moment = require('moment');
require('moment-range');
var async = require("async");
var simplestatistics = require('simple-statistics');

var jwt = require('jwt-simple');
var config = require('../../config/database');



module.exports = (function(){

  'use strict';

  var web = express.Router();



  web.get('/',function(req, res){
      res.redirect('/signin');
  });

  /**
   * ตรวจสอบกาล็อคอิน
   */
  var isAuthenticated = function (req, res, next) {
    if (req.isAuthenticated())
      return next();
    res.redirect('/signin');
  }



  /**
   *  แสดง หน้า login
   */
   web.get('/signin',function(req,res){
     res.render('login.jade',{message : req.flash('message')});
   })

   /**
    *  ออกจากระบบ
    */
   web.get('/signout',function(req,res){
     req.logout();
     res.redirect('/signin');

   })


  /**
   * ตรวจสอบข้อมูลล็อคอิน
   */
   web.post('/signin',passport.authenticate('local',{ successRedirect: '/channel',failureRedirect: '/signin', failureFlash: true }));


   /**
    * แสดงหน้าสร้าง account
    */
   web.get('/register',function(req,res){
     res.render('register.jade');
   })


   /**
    * บันทึกข้อมูล
    */
   web.post('/register',function(req,res){

     var newUser = new User({
       name : req.body.username ,
       password : req.body.password ,
       email : req.body.email
     });

     newUser.save(function(err){
       if (err) {
         res.render('register.jade',{message : "Something wrong Please try "})
       }else{
        res.redirect('/signin');
       }
     });

   })


  /**
   * แสดงข้อมูลบัญชี
   */
  web.get('/',isAuthenticated,function (req,res){
    res.render('test.jade');
  })


  /**
   * แสดงข้อมูลบัญชี
   */
  web.get('/channel',isAuthenticated,function (req,res){
      var user = req.user;
      Channel.find({user : req.user._id },function(err,channels){

          var resutl =  [] ;
          channels.forEach(function(ch){
            resutl.push({
              _id : ch._id,
              name :  ch.name ,
              description : ch.description ,
              createdAt : dateUtils.toShow(ch.createdAt)
            });
          });

        res.render('channel.jade',{ channels : resutl });

      })
    //console.log(user);


  })


  /**
   *  บัยทึกข้อมูลแซนแนล
   */
   web.post('/channel',isAuthenticated,function(req,res){

     var newChannel = new Channel({
       name : req.body.channelname ,
       description : req.body.description,
       user : req.user._id
     });

     newChannel.save(function(err){
       res.redirect('/channel');
     });

   });


    /**
     *  อัพเดรตข้อมูลแซนแนล
     */
     web.post('/channel/update',isAuthenticated,function(req,res){
       Channel.findOneAndUpdate({ _id : req.body.channelid } ,{
         $set: {
                  name : req.body.channelname  ,
                  description : req.body.description
               }
             },
             {

             },
        function(err){
                res.redirect('/channel');
      });

     });

   /**
    *  ลบข้อมูลแซนแนล
    */
   web.get('/channel/delete/:_id',isAuthenticated,function(req,res){
     Channel.findOneAndRemove({ _id: req.params._id},function(err){
       if (err) {
         throw err
       }else{
         res.redirect('/channel')
       }
     });
   });

   /**
    * แสดงข้อมูล แซนแนล
    */
   web.get('/channel/view/:_id',isAuthenticated,function(req,res){
     async.parallel([
       function(cb){
         Channel.findOne({_id : req.params._id})
         .populate('user')
         .exec(function(err,data){
           cb(err,data);
         })
       },
       function(cb){
         Feed.findOne({channel : req.params._id })
         .sort({ 'createdAt' : -1})
         .exec(function(err,data){
           cb(err,data);
         })
       },
       function(cb){
           Feed.count({channel : req.params._id },function(err,data){
             cb(err,data);
           });
       }
     ],function(err,data){
        res.render('channel-view',{
          channel : data[0],
          channel_CreateAt : dateUtils.toShow(typeof data[0]==='undefined'   ?  "" : data[0].createdAt),
          lastFeed : data[1],
          lastFeedCreateAt : dateUtils.toShow(typeof data[0]==='undefined'   ?  "" : data[0].createdAt),
          cn : data[2]

        });
     });
   });

   /**
    *
    */
   web.get('/account/view',isAuthenticated,function(req,res){
      res.render('account-view.jade',{
        token : 'JWT ' + jwt.encode( req.user, config.secret) ,
        user : req.user
      });
   });


   /**
    *
    */
   web.get('/ajax/channel/:_id',function(req,res){
     async.parallel([
       function(cb){
         Channel.findOne({_id : req.params._id})
         .populate('user')
         .exec(function(err,data){
           cb(err,data);
         })
       },
       function(cb){
         Feed.findOne({channel : req.params._id })
         .sort({ 'createdAt' : -1})
         .exec(function(err,data){
           cb(err,data);
         })
       },
       function(cb){
           Feed.count({channel : req.params._id },function(err,data){
             cb(err,data);
           });
       }
     ],function(err,data){



       var now = moment(new Date());
       var _lastFeedCreateAt = new Date(typeof data[1] ==='undefined' ? "" : data[0].createdAt );
       var _lastFeedCreateAt_show =  "";




        res.json({
          channel : data[0],
          channel_CreateAt : dateUtils.toShow( typeof data[0] === 'undefined' ?  "" : data[0].createdAt  ) + "  ( "+ dateUtils.toShow_diff(data[0].createdAt) +")" ,
          lastFeed : data[1],
          lastFeedCreateAt : dateUtils.toShow(typeof data[1] === 'undefined' ? "" : data[1].createdAt ) + "  ( " + dateUtils.toShow_diff(data[1].createdAt) +")" ,
          cn : data[2]
        });
     })
   });

   /**
    *
    */
   web.get('/ajax/channel/:_id/productvity',function(req,res){
     Productvity.find({ channel : req.params._id})
     .sort({ 'createdAt' : -1})
     .exec(function(err,data){
       var newData = [] ;
       data.forEach(function(i,idx){
         newData.push({
           _id: i._id ,
           createdAt: i.createdAt,
           value : i.value ,
           date : dateUtils.toShow_notime(i.date)

         })
       });

       res.json({data : newData});
     });
   });

   /**
    *
    */
   web.post('/ajax/channel/:_id/productvity',function(req,res){

    Productvity.findOne({channel :req.params._id,date :new Date(req.body.productvity_date).toISOString() },function(err,productvity){
      if(productvity){
         res.json({result: false})
      }else{
        var newProductvity = new Productvity({
           value : req.body.productvity_qty,
           date : new Date(req.body.productvity_date).toISOString(),
           channel: req.params._id
         });

         newProductvity.save(function(err){
           res.json({"result" : true});
         });

      }
    });
   });

   /**
    *
    */
   web.post('/ajax/channel/productvity/update',function(req,res){
     Productvity.findOneAndUpdate({ _id : req.body.product_id } ,
       {
         $set : {date : req.body.date,value : req.body.productvity_qty

         }
       },{

       },function(errr){
         res.json({"resut":"succes"});
       });
   });


   /**
    *
    */
    web.get('/ajax/channel/productvity/delete/:_id',function(req,res){
      Productvity.findOneAndRemove({_id: req.params._id},function(err){
        if (err) {
          throw err
        }else{
          res.json({"resut":"succes"})
        }
      });
    });



    /**
     *
     */
     web.get('/ajax/channel/:_id/analyze/:ge/:le',function(req,res){

       var geDate = new Date(req.params.ge);
       var leDate = new Date(req.params.le);
       var rangeDate = moment.range(geDate,leDate);
       var dateRange = rangeDate.toArray('days') ;
       var criteria = {"$gte":new Date( geDate.getFullYear() + ',' +(geDate.getMonth()+1) + ','+ geDate.getDate() ),"$lt": new Date( leDate.getFullYear() + ',' +(leDate.getMonth()+1) + ','+ (leDate.getDate()+1) )} ;

       async.parallel([
        function(cb){
          Feed.find({channel:req.params._id,"createdAt":criteria }, function(err, data){
              cb(err,data);
            });
        },
        function(cb){
          Productvity.find({channel:req.params._id,"createdAt":criteria }, function(err, data){
              cb(err,data);
            });
         }],
      function(err,data){

          var feeds = data[0] ;
          var productvity = data[1] ;

          // h lite per hr .
          // one times  = 10 second


          var categories = [] ;
          var series_h_data = [] ;
          var series_t_data = [] ;
          var series_water_data = [] ;
          var series_p_data = [] ;


          var ds_cal = [];
          for (var i = 0; i < dateRange.length; i++) {
            categories.push(dateUtils.toShow_notime(dateRange[i]));
            var ds_day = [] ;
            var ds_key =  dateUtils.toShow_notime(dateRange[i]) ;
            var _ds_humidity = [] ;
            var _ds_temperature = [] ;
            var _waterOpenTime = 0 ;
            var _ds_productvity = [] ;

            // feed
            for (var j = 0; j < feeds.length; j++) {
              if ( dateUtils.toShow_notime(feeds[j].createdAt) === dateUtils.toShow_notime(dateRange[i]) ) {
                _ds_humidity.push(feeds[j].humidity);
                _ds_temperature.push(feeds[j].temperature);

                if(feeds[j].humidity < 80  || feeds[j].temperature > 33){
                  _waterOpenTime++;
                }

              }
            }

            // productvity
            for (var k = 0; k < productvity.length; k++) {
              if ( dateUtils.toShow_notime(productvity[k].date) === dateUtils.toShow_notime(dateRange[i]) ) {
                _ds_productvity.push(productvity[k].value);
              }
            }

            series_h_data.push(_ds_humidity.length> 0 ?simplestatistics.mean(_ds_humidity): [0]);
            series_t_data.push(_ds_temperature.length>0 ?  simplestatistics.mean(_ds_temperature) : [0]);
            series_water_data.push((((_waterOpenTime * 10) /60 ) /60 ) * 1000  ) ;
            series_p_data.push(_ds_productvity.length > 0 ? _ds_productvity : [0] );
            //console.log(_ds_productvity);
          /*
            ds_cal.push(
              {
                date : ds_key ,
                dataSet : {
                  humidity : _ds_humidity,
                  temperature : _ds_temperature ,
                  productvity : _ds_productvity

                },
                statisticsValue : {
                  humidity :{
                      max : simplestatistics.max(_ds_humidity) ,
                      min : simplestatistics.min(_ds_humidity) ,
                      mean : simplestatistics.mean(_ds_humidity)
                  },
                  temperature : {
                    max : simplestatistics.max(_ds_temperature) ,
                    min : simplestatistics.min(_ds_temperature) ,
                    mean : simplestatistics.mean(_ds_temperature)
                  },
                  productvity :{
                    max : simplestatistics.max(_ds_productvity) ,
                    min : simplestatistics.min(_ds_productvity) ,
                    mean : simplestatistics.mean(_ds_productvity)
                  },
                  resourceUsed : {
                    water : (((_waterOpenTime * 10) /60 ) /60 ) ,
                  }
                }

              }
            );
            */


          }



          res.json({
            "range" :rangeDate.diff('days') ,
            "rang-date" : dateRange,
            "analyze": ds_cal,
            "chart" :{
              series : [
                {
                  name : 'ความชื้น (%)' ,
                  data : series_h_data
                },
                {
                  name : 'อุณหภูมิ (c)',
                  data : series_t_data
                },
                {
                  name : "การใช้น้ำ (ml.)",
                  data : series_water_data

                },
                {
                  name : "ผลผลิต (g)",
                  data : series_p_data
                }
              ],
              categories : categories
            }

          });




      });
     });


  return web ;


})();
