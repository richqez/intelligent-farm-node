var Channel = require('../models/channel');
var express = require('express');
var userAuth = require('../lib/userAuth');
var User = require('../models/users');
var passport = require('passport');
var config = require('../../config/database');

module.exports = (function(){

	'use strict';

	var channel = express.Router();


  var isAuthenticated = function (req, res, next) {
    if (req.isAuthenticated())
      return next();
    res.redirect('/signin');
  }


 	channel.post('/channel/add',passport.authenticate('jwt', { session: false}),  function(req, res){

 	var token  = userAuth.verify(req.headers);

		if(!req.body.name){

			res.json({success: false, msg: 'Please pass name'});

		}else{

			var newCH = new Channel({
				name : req.body.name,
				description : req.body.description,
				user : req.body.user,
				timestamps : req.body.timestamps
			});

			newCH.save(function(err){
				if(err){
					return res.json({success: false, msg: 'Channel already exists.'});
				}
					res.json({success: true, msg: 'Successful created new Chanel.'});
			});
		}
	})


	 channel.get('/channel/findAll',passport.authenticate('jwt', { session: false}), function(req, res){

	 	var token  = userAuth.verify(req.headers);

	 	Channel.find({}, function(err, channels){
	 		res.json(channels);
	 	});
	 })


	 channel.post('/channel/findByUserId',passport.authenticate('jwt', { session: false}), function(req, res){

		var token  = userAuth.verify(req.headers);

	 	Channel.find({user:req.body.user}, function(err, channels){
	 		res.json(channels);
	 	});
	 });

	 channel.post('/channel/delete',passport.authenticate('jwt', { session: false}), function(req, res){

		var token  = userAuth.verify(req.headers); 	

	 	Channel.findOneAndRemove({id:req.body.id}, function(err){

	 		if (err) {

	 			throw err;

	 		}else{

	 			return res.json({success: false, msg: 'Delete Complete'});
	 		}	
	 	});

	 });

	  channel.post('/channel/update',passport.authenticate('jwt', { session: false})	, function(req, res){

		var token  = userAuth.verify(req.headers);
	  	
	  	Channel.findOneAndUpdate({_id:req.body.id},{$set: {name:req.body.name}},{new: true}, function(err, user) {
	  	if (err){
	  		throw err;
	  	} else{
	  		return res.json({success: false, msg: 'Update Complete'});
	  	}
	  
	  });
	 });



	   channel.get('/devchannel', function(req, res){

	 	 	Channel.find({}, function(err, channels){
	 		res.json(channels);
	 	});
	 })

 return channel;

})();

