var config = require('../../config/database');
var User = require('../models/users');
var jwt = require('jwt-simple');



module.exports = (function(){

  'use strict';

 var userAuth = {};

 userAuth.verify = function(tokenheader){
    var token = getToken(tokenheader);
    if (token) {
        var decoded = jwt.decode(token, config.secret);
        return decoded;
    }
    else{
      return null ;
    }

 }

 userAuth.getToken = function (headers) {
    if (headers && headers.authorization) {
      var parted = headers.authorization.split(' ');
      if (parted.length === 2) {
        return parted[1];
      } else {
        return null;
      }
    } else {
      return null;
    }
  }


  return userAuth ;


})();
