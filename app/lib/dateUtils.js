var moment = require('moment');
require('moment-range');


module.exports = (function(){

  'use strict';

  var dateUtils = {};

  dateUtils.toShow = function(dateIso){

    if (dateIso === "" || dateIso === null || dateIso  === "undefined") {
      return ""
    }

    var date = new Date(dateIso);
    var h  = date.getUTCHours()+7    ;
    if(h > 24){
      h = h - 24 ;
    }

    return date.getFullYear()+'-' + (date.getMonth()+1) + '-'+date.getDate() + ' ' + h + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds();
  }

  dateUtils.toShow_notime = function(dateIso){

    if (dateIso === "" || dateIso === null || dateIso  === "undefined") {
      return ""
    }

    var date = new Date(dateIso);
    var h  = date.getUTCHours()+7    ;
    if(h > 24){
      h = h - 24 ;
    }

    return date.getFullYear()+'-' + (date.getMonth()+1) + '-'+date.getDate() ;
  }

  dateUtils.toShow_diff = function(dateIso){

    var now = moment(new Date());
    var input = moment(new Date(dateIso));

    if (now.diff(input,'weeks') != 0 ) {

      return  now.diff(input,'weeks') > 1 ?   now.diff(input,'weeks') + " weeks ago"  :  now.diff(input,'weeks') + " week ago" ;

    }else if (now.diff(input,'days') != 0 ) {

      return  now.diff(input,'days') > 1 ?   now.diff(input,'days') +  " days ago"  :  now.diff(input,'days') +  " day ago" ;

    }else if (now.diff(input,'hours') != 0) {

      return  now.diff(input,'hours') > 1 ?  now.diff(input,'hours') +  " hours ago"  : now.diff(input,'hours') +  " hour ago" ;

    }else if (now.diff(input,'minutes') != 0) {

      return   now.diff(input,'minutes') > 1 ?   now.diff(input,'minutes')  + " minutes ago"  : now.diff(input,'minutes')  + " minute ago" ;

    }else{
      return "less than a minutes";
    }
  }

  dateUtils.deleteTime = function(dateIso){

    var d = new Date(dateIso);
      d.setHours(0, 0, 0, 0);

    return d ;

  }




  return dateUtils;


})();
