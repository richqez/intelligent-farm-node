function ViewModel_1(){

  var self = this;
  this.chName = ko.observable();
  this.chDes = ko.observable();
  this.chCreateAt = ko.observable();
  this.chLE  = ko.observable();
  this.chCN = ko.observable();
  this.lastH = ko.observable();
  this.lastT = ko.observable();
  this.device_status = ko.observable();
  this.device_ip = ko.observable();




  self.loadData = function (){

    $.get('../../ajax/channel/'+channel_id,function(res){
            self.chName(res.channel.name) ;
            self.chDes(res.channel.description);
            self.chCreateAt(res.channel_CreateAt);
            self.chLE(res.lastFeedCreateAt);
            self.chCN(res.cn);
            self.lastH(parseFloat(Math.round(res.lastFeed.humidity * 100) / 100).toFixed(2) +" %");
            self.lastT(parseFloat(Math.round(res.lastFeed.temperature * 100) / 100).toFixed(2) + " c");

          });
    }

    self.loadData();
    setInterval(self.loadData, 10000);
}





function ViewModel_2(){

  var self = this ;

  this.productvity_qty = ko.observable()
  this.productvity_date = ko.observable()

  this.productvity_Table = $("#productvity_table").DataTable({
      columnDefs : [{
                      targets : 'no-sort',
                      orderable : false ,
                      order : []
                    }],
      ajax : '/ajax/channel/'+channel_id+'/productvity',
      columns : [
        { data : "date" },
        { data : "value" },
        { mRender : function(data,type,full){
            return '<Button btnproductedit class="btn btn-info" data-key="'+full._id+'"  data-value="'+full.value+'" data-date="'+full.date+'" > <i class="fa fa-pencil"></i> </Button> <Button btnproduct_Delete class="btn btn-danger" data-key="'+full._id+'"> <i class="fa fa-remove"> </i> </Button>'
        }}

      ]
  });


  self.productvitySave = function(){

    if(true) {
      $.post('../../ajax/channel/'+channel_id+'/productvity',{
      productvity_qty:self.productvity_qty,
      productvity_date:self.productvity_date},function(res){

      if(res.result){


        swal("Save Success", "", "success")
        self.productvity_Table.ajax.reload();
      }else{
        swal("Can not Save!", "", "error");
      }

        self.productvity_qty("");
        self.productvity_date("");

      console.log(res)
      });
    }


  }




}






function fetchData(channel_id){
  $.get('../../api/feed/channel/'+channel_id+'/forchart/',chartUpdate);
}
var chartUpdate = function(value) {
  value = jQuery.parseJSON(JSON.stringify(value));
    window.chartOptions = {
     segmentShowStroke: false,
     percentageInnerCutout: 75,
     animation: false
   };
  var ctx =  $("#ch_chart");
  var data = {
    labels: value.labels,
    datasets:[
      {
        label: "Temperature",
        fill: false,
        lineTension: 0.1,
        backgroundColor: "#FABE28",
        borderColor: "#FABE28",
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: "#FA2A00",
        pointBackgroundColor: "#FABE28",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "#FABE28",
        pointHoverBorderColor: "#FABE28",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 5,
        data: value.temperature
      },
      {
        label: "Humidity",
        fill: false,
        lineTension: 0.1,
        backgroundColor: "#1ABC9C",
        borderColor: "#1ABC9C",
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: "#FA2A00",
        pointBackgroundColor: "#FABE28",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(75,192,192,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 5,
        data: value.humidity
      }
    ]
  };
  var myLineChart = Chart.Line(ctx, {
      data: data,
      options: {
        scales:{
          yAxes:[{
            ticks:{
              max : 200,
              min : 0,
              beginAtZero : true

            }
            }]
        }
      }
  });
}





$(document).ready(function() {



  $(document).on('click','button[btnproductedit]', function(){

    var self = $(this) ;

    $('#modalUpdate').modal('toggle');
    $('#productvity_qty_Update').val(self.data('value'))
    $('#dateUpdate').val(self.data('date'))
    $('#product_id_Update').val(self.data('key'))


  });


  $(document).on('click','button[btnproduct_Delete]', function(){

    var self = $(this)

    $.get('/ajax/channel/productvity/delete/'+self.data('key'),function(product){
      channelViewVM_product.productvity_Table.ajax.reload();
    })

  })






  // initialize VM
  var channelViewVM =  new ViewModel_1() ;
  var channelViewVM_product = new ViewModel_2();
  //var channelViewVM_Analytics = new ViewModel_3();

  ko.applyBindings(channelViewVM,document.getElementById("info"));
  ko.applyBindings(channelViewVM_product,document.getElementById("Productvity"));
  //ko.applyBindings(channelViewVM_Analytics,document.getElementById("Analytics"));
  $('#btn_product_update').click(function(){
    var date = $('#dateUpdate').val()
    var productvity_qty = $('#productvity_qty_Update').val()
    var product_id = $('#product_id_Update').val()


    if(!isNaN(productvity_qty)){
     $.post('/ajax/channel/productvity/update',{
       date  : date ,
       productvity_qty :productvity_qty,
       product_id : product_id
     },function(product){
      channelViewVM_product.productvity_Table.ajax.reload();
     })
    }else{

    }


  })



    // initialize datepicker
    $('#date').datepicker({
    });

    $('#dateUpdate').datepicker({});


    // initialize chart JS data
    //fetchData(channel_id);
    /*setInterval(function(){
      fetchData(channel_id);
    }, 150000);
    */

    Highcharts.setOptions({
            global: {
                useUTC: false
            }
        });

    function initChartR(data){

     $('#container-h').highcharts({
          chart: {
              type: 'line',
              zoomType: 'x'
          },
          colors:[
            '#1ABC9C','#ffb400'
          ],
          title: {
              text: 'อุณหภูมิ & ความชื้น'
          },
          xAxis: {
              categories: data.categories
          },
          yAxis: {
              title: {
                  text: 'ความชื้น (C)  อุณหภูมิ (%)'
              }
          },
          series: data.series
      });


    }
    function chartR(){
      $.get('../../api/feed/channel/'+channel_id+'/forchart/v2',function(data){

        initChartR(data)

      })
    }

  chartR()
   setInterval(function(){
     chartR()
   },20000)


    function checkStatus(_ws){
      _ws.send("isonline:"+channel_id+"---endkey---");
      _ws.send("getip:"+channel_id+"---endkey---");
    }


    // initialize WebSocket
    if("WebSocket" in window){

      try{
        console.log("WebSocket initialize...");
        var ws = new WebSocket("ws://103.58.149.179:7777");
        console.log("WebSocket connected");

        ws.onerror = function(error){
          alert("Device control Service has a prob...");
          $('input[name="my-checkbox"]').bootstrapSwitch('toggleDisabled');
        }

        ws.onopen = function(){

          ws.send("isonline:"+channel_id+"---endkey---");
          ws.send("getip:"+channel_id+"---endkey---");
          setInterval(function(){
            checkStatus(ws);
          },10000)



        }

        ws.onmessage = function (e) {
          var status_index = e.data.indexOf('device_status:');
          if (status_index!= -1) {
            var status = e.data.substring("device_status".length+1,e.data.length) ;
            channelViewVM.device_status(status);
            if(status_index == 0){
              if(status == 'offline'){
                $('input[name="my-checkbox"]').bootstrapSwitch('toggleDisabled');

              }
            }
          }
          var ip_index = e.data.indexOf('?ip=');
          if (ip_index != -1) {
            var ip = e.data.substring("?ip=".length+1,e.data.length) ;
            channelViewVM.device_ip(ip);
          }

          console.log('Received From Server: ' + e.data); //log the received message
        };



        $('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function(event, state) {
          var self = $(this);
          if(state){
            ws.send("sendto:"+channel_id+"---endkey---"+' {"water":"on"}');
            self.bootstrapSwitch('toggleDisabled',true,true);
            setTimeout(function(){
              self.bootstrapSwitch('toggleDisabled');
              self.bootstrapSwitch('state', false);
              },10000);;
          }

        });



      }
      catch(err){
        //console.log(err);
        alert(err);
      }



    }else{
      alert("DEVICE CONTROL  NOT supported by your Browser!");
    }



    /*
    var Analytics_data  = {};
    var Analytics_series = [];
    var Analytics_h  = [] ;
    var Analytics_t = [];
    */


    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    cb(moment().subtract(29, 'days'), moment());

    $('#reportrange').daterangepicker({
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);


    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
      console.log(picker.startDate.format('YYYY-MM-DD'));
      console.log(picker.endDate.format('YYYY-MM-DD'));
      getAnalyticsData(picker.startDate.format('YYYY-MM-DD'),picker.endDate.format('YYYY-MM-DD'));
    });

    function initChart(data){

      $('#analytics_container').highcharts({
          title: {
              text: 'เปรียบเทียบ ความชื้น อุณภูมิ การใช้น้ำ ผลผลิต'
          },
          xAxis: {
              categories: data.categories
          },
          yAxis: {
              title: {
                  text: ''
              }
          },
          legend: {
         layout: 'vertical',
         align: 'right',
         verticalAlign: 'middle',
         borderWidth: 0
     },
          series: data.series
      });


    }


    function getAnalyticsData(start,end){
      var url = '../../ajax/channel/'+channel_id+'/analyze/'+start+'/'+end;
      $.get(url,function(data){
        console.log(data);
        initChart(data.chart);



      })
    }




















































});
