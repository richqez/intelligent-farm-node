$('#ch_table').DataTable({
    columnDefs : [{
                    targets : 'no-sort',
                    orderable : false ,
                    order : []
                  }]
});


$(document).on('click', 'a[delete-btn]', function(event) {
  var link = $(this);
  event.preventDefault();
  swal({
    title: "Are you sure?",
    text: "You will not be able to recover this channel ",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, delete it!",
    cancelButtonText: "No, cancel plx!",
    closeOnConfirm: false,
    closeOnCancel: false },
    function(isConfirm){
      if (isConfirm) {
          window.location = link.attr('href');
        swal("Deleted!", "channel has been deleted.", "success");

      } else {
        swal("Cancelled", "channel is safe :)", "error");
       } });

});


$(document).on('click', 'a[update-btn]', function(event) {
  event.preventDefault();
  var ch = $(this);

  $('#ch_name_update').val('');
  $('#ch_des_update').text('');
  $('#ch_id_update').val('')

  $('#ch_name_update').val(ch.data('name'));
  $('#ch_des_update').text(ch.data('des'));
  $('#ch_id_update').val(ch.data('id'))


  $('#modalSuccess').modal('show');

});
