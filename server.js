var express = require('express');
var path = require('path');
var app =	express();
var bodyParser  = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');
var passport = require('passport');
var config = require('./config/database');
var User = require('./app/models/users');
var Channel = require('./app/models/channel');
var port = process.env.PORT || 9898;
var jwt = require('jwt-simple');
var session = require('express-session')
var cookieParser = require('cookie-parser')
var flash = require('connect-flash');
var async = require("async");
var compression = require('compression');



var LocalStrategy = require('passport-local').Strategy;

var CH = require('./app/routes/channel');
var userR = require('./app/routes/users');
var feedR = require('./app/routes/feed');
var webRoute = require('./app/routes/web');


require('./config/passport')(passport);

var apiRoutes = express.Router();

var mongo_express = require('mongo-express/lib/middleware');
var mongo_express_config = require('./config/xn');



mongoose.connect(config.database);
mongoose.set('debug', true);

app.use(compression())
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.set('trust proxy', 1)
app.use(cookieParser())
app.use(session({
  secret: 'xzcxsad',
  resave: false,
  saveUninitialized: true,
}))

app.use('/mongo_express', mongo_express(mongo_express_config))


app.use(express.static(path.join(__dirname, '/public')));
app.use(express.static(path.join(__dirname, '/public/')));


app.use(bodyParser.urlencoded({ extended : false }));
app.use(bodyParser.json());

app.use(morgan('dev'));
app.use(flash());


app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(
  {passReqToCallback: true},
  function(req,username, password, done) {
    User.findOne({name: username}, function(err, user) {

      if (err) throw errr

      if (!user) {
          req.flash("message","Authentication failed. User not found..")
        return done(null, false, { message: 'Authentication failed. User not found..' });
      }else {
        // check if password matches
        user.comparePassword(password, function (err, isMatch) {
  	        if (isMatch && !err) {
  	          return done(null, user);
  	        } else {
              req.flash("message","Incorrect password")
  	          return done(null, false, { message: 'Incorrect password.' });
  	        }
  	      })
      }
    });
  }
));

passport.serializeUser(function(user, done) {
  done(null, user._id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});

/**
 *  Helper
 */

 app.use(function(req,res,next){
   res.locals.session =  req.session;
   res.locals.currentuser = req.user;
   next();
 })

/**
 *  Web app ROUTE
 */




 app.use('/',webRoute);
 app.use('/api',userR);
 app.use('/api',feedR);
 app.use('/api',CH);



















app.listen(port);
console.log('There will be localhost'+port);
